package co.com.sura.pruebatecnica.servicios;

public class Mailer {
	public boolean sendEmail(String strFrom, String strTo, String strSubject, String strText){
		
		System.out.println("Enviando email ...");
		System.out.println("From: "+strFrom);
		System.out.println("To: "+strTo);
		System.out.println("Subject: "+strSubject);
		System.out.println("Text: "+strText);
		
		return true;
	}
}
