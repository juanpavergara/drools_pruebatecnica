package co.com.sura.pruebatecnica.servicios;

import java.util.ArrayList;
import java.util.List;

import org.drools.runtime.StatelessKnowledgeSession;

import co.com.sura.droolsIntegrationServices.exceptions.SuraDroolsIntegrationServicesException;
import co.com.sura.droolsIntegrationServices.manager.StatelessRulesManager;
import co.com.sura.pruebatecnica.modelo.*;

public class ServicioDummy {
	
	private StatelessKnowledgeSession getDroolsExpertSession(String istrChangeSet){
		try{
			String strNombreChangeset = istrChangeSet;
			System.out.println("Compilando reglas ...");
			long t1 = System.currentTimeMillis();
			StatelessRulesManager.init(strNombreChangeset, true);
			long t2 = System.currentTimeMillis();
			System.out.println("Reglas compiladas ...");
			System.out.println("Tiempo de compilacion de reglas: "+ (t2-t1) + " ms");
	    	
	    	
			System.out.println("Obteniendo manager ...");
	    	StatelessRulesManager manager = StatelessRulesManager.getManager(strNombreChangeset);
	    	System.out.println("Manager obtenido ...");
	    	
	    	System.out.println("Obteniendo sesi�n de conocimiento ...");
	    	StatelessKnowledgeSession session = manager.getSession();
	    	System.out.println("Sesi�n de conocimiento obtenida ... ");
	    	
	    	return session;
	    	
    	}catch(SuraDroolsIntegrationServicesException e){
    		//TODO: Aca el desarrollador debe controlar qu� hacer si ocurrio la excepcion que indica que hubo
    		// error en la compilacion de recursos "en caliente"
    		e.printStackTrace();
    		return null;
    		
    	}catch(Exception e){
    		System.out.println("Ocurrio una excepcion en la prueba de Guvnor");
    		e.printStackTrace();
    		return null;
    	}
	}
	
	public void ejecutarReglas(){
		String strNombreChangeset = "co/com/sura/pruebatecnica/changesets/pruebatecnica_changeset.xml";
		StatelessKnowledgeSession session = getDroolsExpertSession(strNombreChangeset);
		Mailer mailer = new Mailer();
		
		List hechos = new ArrayList();	
		hechos.add(new HechoDos());
		hechos.add(new HechoUno());
		hechos.add(new HechoCuatro());
		
		session.setGlobal("globalMailer", mailer);
		
		session.execute(hechos);
	}
	
	public static void main(String[] args) {
		ServicioDummy servicio = new ServicioDummy();
		servicio.ejecutarReglas();
	}
}
